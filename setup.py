from distutils.core import setup

setup(
    name='dbadminserver',
    version='1.0.0',
    packages=[''],
    url='https://gitlab.com/paxton.dev/dbadminserver.git',
    license='',
    author='paxton.dev',
    author_email='paxton.m05@gmail.com',
    description='Python Flask REST API for the DBAdmin Web Framework'
)
