import unittest

from dbadminserver.blue import get_config

class BlueTest(unittest.TestCase):

    def test_positive_get_config(self):
        # Attempt to retrieve the URL configuration file
        config_json = get_config('routes.cfg')

        # Ensure that the configuration file is in the expected format
        self.assertEqual(type({}), type(config_json))

    def test_negative_get_config(self):
        # Ensure that a FileNotFoundError is raised
        with self.assertRaises(FileNotFoundError):
            # Attempt to retrieve an invalid configuration file
            config_json = get_config('invalid.cfg')

if __name__ == '__main__':
    unittest.main()
