# External Imports
from flask import Flask
from flask_cors import CORS
import json
import os

# Internal Imports
from dbadminserver.blue import api_blue

# Define some directories for global use
ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
CFG_DIR = os.path.join(ROOT_DIR, 'configuration')


def create_application(config):
    """
    Create the Flask application
    :param config: the configuration object in dictionary format
    :return app: the created application object
    """
    app = Flask(__name__)
    CORS(app)
    app.register_blueprint(api_blue, url_prefix=config['url_prefix'])
    app.config['env'] = config['env']
    return app


def get_config(config_name):
    """
    Retrieve the specified configuration file
    :param config_name: The name of the config file to be retrieved
    :return config_json: The retrieved JSON configuration object
    """
    config_path = os.path.join(CFG_DIR, config_name)

    with open(config_path, 'r') as myfile:
        data = myfile.read()

        # parse file
        config_json = json.loads(data)
        return config_json['dev']

# Make a global reference to app
app = None

if __name__ == '__main__':
    config = get_config('setup.cfg')
    app = create_application(config)
    app.run(
        host='0.0.0.0',
        port=config['port'],
        debug=config['debug']
    )
