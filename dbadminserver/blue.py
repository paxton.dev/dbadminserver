# External Imports
from flask_restful import Api, Resource
from flask import Blueprint, jsonify
import json
import os

# Define configuration directory
ROOT_DIR = os.path.dirname(os.path.dirname(__file__))
CFG_DIR = os.path.join(ROOT_DIR, 'configuration')

# Create the Flask Blueprint
api_blue = Blueprint('api', __name__)


def to_json(results, column_names):
    pass


def format_tables(table_string, command):
    pass


def query(data, param=None):
    pass


def popualte_homepage(param=None, name=None, data=None):
    return data


def populate_route(param=None, name=None, data=None):
    if not data["technology"]:
        return jsonify({"response": data["source"]})
    elif data["technology"] == "file":
        # Locate the configuration file via the project root
        config_path = os.path.join(CFG_DIR, data["source"])

        with open(config_path) as config_file:
            config_json = json.load(config_file)
            return jsonify({"response": config_json[param]})
    else:
        return jsonify({"response": data["description"]})


def create_routes(config_json):
    for route in config_json:
        if route == 'home':
            api_blue.add_url_rule(
                config_json[route]["url"],
                view_func=popualte_homepage,
                endpoint=route,
                defaults={'name': route, 'data': config_json}
            )
        else:
            api_blue.add_url_rule(
                config_json[route]["url"],
                view_func=populate_route,
                endpoint=route,
                defaults={'name': route, 'data': config_json[route]}
            )


def get_config(file_name):
    """
    Retrieve the configuration file
    :param file_name: the name of the configuration file to retrieve
    :return config_json: the json object containing route_config
    """
    # Locate the configuration file via the project root
    config_path = os.path.join(CFG_DIR, file_name)

    # Open the configuration file
    try:
        with open(config_path) as config_file:
            config_json = json.load(config_file)
            return config_json
    except FileNotFoundError:
        raise

# Load the configuration file
config_json = get_config('routes.cfg')

# Populate the API routes
create_routes(config_json)

# Populate the API with the blueprint
Api = Api(api_blue)
